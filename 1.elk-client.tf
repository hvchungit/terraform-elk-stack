resource "aws_instance" "elk-client" {
    # The connection block tells our provisioner how to
    # communicate with the resource (instance)
    
    instance_type = "t3a.small"
    
    ami = var.ami-id
    
    key_name = var.keyname
    
    associate_public_ip_address = true

    subnet_id = data.aws_subnet.selected.id

    vpc_security_group_ids = [ "${aws_security_group.elk_hufi_client.id}" ]
    
    tags = {
        Name        = "ELK Client"
        Workspace = "hufi"
    }
}

resource "null_resource" "provision-client"{
    
    connection{
        type = "ssh"
        host = aws_instance.elk-client.public_ip
        user = var.user
        private_key = file("${var.keypath}")   
    }
    
    provisioner "remote-exec"{
        inline = [
                    "sudo bash -c 'echo \"ELK_SERVER=${aws_instance.elk-server.private_ip}\" >> /etc/environment'"
        ]
    }
    
    provisioner "file"{
        source = "scripts/install_filebeat.sh"
        destination = "/tmp/install_filebeat.sh"
    }

    provisioner "file"{
        source = "configs/filebeat.yml"
        destination = "/tmp/filebeat.yml"
    }

    provisioner "file"{
        source = "configs/vsftpd.conf"
        destination = "/tmp/vsftpd.conf"
    }
    
    provisioner "remote-exec"{
        inline = [
                    "chmod +x /tmp/install_filebeat.sh",
                    "sudo sh /tmp/install_filebeat.sh ${aws_instance.elk-server.private_ip}" 
        ]
    }
    
    depends_on = [
        null_resource.provision-server,
        aws_instance.elk-client
    ]
    
}

