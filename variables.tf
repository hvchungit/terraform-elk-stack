variable "region" {
    default = "ap-southeast-1"
}

variable "profile" {
    default = "chungtvv"
}

variable "keyname" {
    default = "ec2_hdh_linux"
}

variable "keypath" {
    default = "/Users/chung/Desktop/tmp/ec2_hdh_linux.pem"
}

variable "ami-id" {
    default = "ami-0d058fe428540cd89"
}

variable "user" {
    default = "ubuntu"
}