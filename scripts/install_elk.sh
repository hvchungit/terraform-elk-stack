sudo apt-get update -y
sudo apt-get install openjdk-8-jre-headless apt-transport-https wget nginx -y
wget -qO - https://artifacts.elastic.co/GPG-KEY-elasticsearch | sudo apt-key add -
echo "deb https://artifacts.elastic.co/packages/7.x/apt stable main" | sudo tee -a /etc/apt/sources.list.d/elastic-7.x.list
sudo apt update -y
sudo apt-get install elasticsearch=7.12.1 -y
# Overwrite default Elasticsearch setting
sudo cp -f /tmp/elasticsearch.yml /etc/elasticsearch/
# Enable start with system and start service
sudo systemctl enable elasticsearch
sudo systemctl start elasticsearch


######## Install Kibana ###########
sudo apt install kibana=7.12.1 -y
sudo cp -f /tmp/kibana.yml /etc/kibana/
sudo systemctl enable kibana
sudo systemctl start kibana
# Setup authenticator for Kibana
echo "admin:`openssl passwd -apr1 kibanaadmin`" | sudo tee -a /etc/nginx/htpasswd.users



######### Setup Nginx Reverse Proxy ########
sudo cp -r /tmp/elk-hufi.com /etc/nginx/sites-available/elk-hufi.com
sudo ln -s /etc/nginx/sites-available/elk-hufi.com /etc/nginx/sites-enabled/elk-hufi.com
sudo systemctl restart nginx


######### Install Logstash #########
sudo apt install logstash=1:7.12.1-1 -y
sudo cp -f /tmp/02-beats-input.conf /etc/logstash/conf.d/
sudo cp -f /tmp/10-syslog-filter.conf /etc/logstash/conf.d/
sudo cp -f /tmp/30-elasticsearch-output.conf /etc/logstash/conf.d/
sudo systemctl start logstash
sudo systemctl enable logstash

