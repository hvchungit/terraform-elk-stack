elkserver=$1
sudo apt-get install update -y
wget -qO - https://artifacts.elastic.co/GPG-KEY-elasticsearch | sudo apt-key add -
echo "deb https://artifacts.elastic.co/packages/7.x/apt stable main" | sudo tee /etc/apt/sources.list.d/elastic-7.x.list
sudo apt update -y
sudo apt install filebeat=7.12.1 -y
# Install FTP server
sudo apt install vsftpd -y
# Config VSFTPD server
sudo cp -r /tmp/vsftpd.conf /etc/

# Enable firewall + ftp server
# sudo ufw enable
# sudo ufw allow 20/tcp
# sudo ufw allow 21/tcp
# sudo ufw allow 22/tcp
# sudo ufw allow 40000:50000/tcp
# Add FTP server user
useradd -m ftpuser
sudo chpasswd << 'END'
ftpuser:abc123
END
sudo mkdir /home/ftpuser/ftp
sudo chown nobody:nogroup /home/ftpuser/ftp
sudo chmod a-w /home/ftpuser/ftp
sudo mkdir /home/ftpuser/ftp/files
sudo chown ftpuser:ftpuser /home/ftpuser/ftp/files
sudo cat > /etc/vsftpd.userlist << EOF
ftpuser
EOF
echo "vsftpd test file" | sudo tee /home/ftpuser/ftp/files/test.txt

sudo systemctl enable vsftpd
sudo systemctl restart vsftpd

# Install filebeat configuration file
sudo cp -r /tmp/filebeat.yml /etc/filebeat/

# Enable Filebeat system module
sudo filebeat modules enable system
sudo filebeat setup --template -E output.logstash.enabled=false -E "output.elasticsearch.hosts=[\"${elkserver}:9200\"]"
sudo filebeat setup -e -E output.logstash.enabled=false -E output.elasticsearch.hosts=["${elkserver}:9200"] -E setup.kibana.host=${elkserver}:5601

# Setup Env for filebeat conf
sudo mkdir /lib/systemd/system/filebeat.service.d
sudo cat > /lib/systemd/system/filebeat.service.d/local.conf << EOF
[Service]
Environment="ELK_SERVER=${elkserver}"
EOF
systemctl daemon-reload
sudo systemctl start filebeat
sudo systemctl enable filebeat