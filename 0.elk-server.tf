resource "aws_instance" "elk-server" {
    # The connection block tells our provisioner how to
    # communicate with the resource (instance)
    
    instance_type = "t3a.large"
    
    ami = var.ami-id
    
    key_name = var.keyname
    
    associate_public_ip_address = true

    subnet_id = data.aws_subnet.selected.id
    
    vpc_security_group_ids = [ "${aws_security_group.elk_hufi_server.id}" ]
    
    
    tags = {
        Name        = "ELK Server"
        Workspace = "hufi"
    }
    
    
}

resource "null_resource" "provision-server"{
    
    connection{
        type = "ssh"
        host = aws_instance.elk-server.public_ip
        user = var.user
        private_key = file("${var.keypath}")   
    }
    
    provisioner "file"{
        source = "scripts/install_elk.sh"
        destination = "/tmp/install_elk.sh"
    }
    
    provisioner "file"{
        source = "configs/02-beats-input.conf"
        destination = "/tmp/02-beats-input.conf"
    }
    
    provisioner "file"{
        source = "configs/10-syslog-filter.conf"
        destination = "/tmp/10-syslog-filter.conf"
    }
    
    provisioner "file"{
        source = "configs/30-elasticsearch-output.conf"
        destination = "/tmp/30-elasticsearch-output.conf"
    }

    provisioner "file"{
        source = "configs/elasticsearch.yml"
        destination = "/tmp/elasticsearch.yml"
    }

    provisioner "file"{
        source = "configs/kibana.yml"
        destination = "/tmp/kibana.yml"
    }

    provisioner "file"{
        source = "configs/elk-hufi.com"
        destination = "/tmp/elk-hufi.com"
    }
    
    provisioner "local-exec" {
      command = "sleep 5"
    }
    
    provisioner "remote-exec"{
        inline = [
                    "chmod +x /tmp/install_elk.sh",
                    "sudo sh /tmp/install_elk.sh"
        ]
    }
    
    depends_on = [
        aws_instance.elk-server
    ]
    
}

